***************************
Project Management Classics
***************************

Born around 1880s, scientific management is a theory of management that analyzes and synthesizes workflows. Its main objective is improving economic efficiency, especially labor productivity. It was one of the earliest attempts to apply science to the engineering of processes and to management. Scientific management is sometimes known as Taylorism after its founder, Frederick Winslow Taylor.

Although scientific management as a distinct theory or school of thought was obsolete by the 1930s, most of its themes are still important parts of industrial engineering and management today.

For exemple, the GANTT Chart, developped in the 1910s by Henry Laurence Gantt (1861 - 1919), also a main leader of the scientific management movement, is now used and abused everywhere.

In the same way emerged in the 1950s the Critical Path Method and PERT

Incidently, in 1969 the Project Management Institute was launched to promote the project management profession.

The concept of the V-Model was developed simultaneously, but independently, in Germany and in the United States in the late 1980s. It is roughly equivalent to PRINCE2.



PERT
====


Program evaluation and review technique


.. image:: images/PertChart.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: PERT Chart
    
    
PERT offers a management tool, which relies "on arrow and node diagrams of activities and events: arrows represent the activities or work necessary to reach the events or nodes that indicate each completed phase of the total project."

It is mostly used for Projects where the main constraint to focus on is Time.

Terminology
-----------


Events and Activity:
  - **PERT event**: a point that marks the start or completion of one or more activities. It consumes no time and uses no resources. When it marks the completion of one or more activities, it is not "reached" (does not occur) until all of the activities leading to that event have been completed.
  - **PERT activity**: the actual performance of a task which consumes time and requires resources (such as labor, materials, space, machinery). It can be understood as representing the time, effort, and resources required to move from one event to another. A PERT activity cannot be performed until the predecessor event has occurred.
  
Time
----

PERT has defined four types of time required to accomplish an activity: 
 - Optimistic Time (o)
 - Pessimistic Time (p)
 - Most likely Time (m)
 - Expected Time. Assuming we repeat the task 6 times, we have te = (o + 4m + p) /6
 - Standard deviation of time. sigma(te) = (p-o) / 6
 
 
Management Tools
----------------

There are a few tools that can be used for management:
 - slack : excess of time and resources available for the task
 - critical path: the longest possible continuous pathway taken from the initial event to the terminal event. It determines the total calendar time required for the project; and, therefore, any time delays along the critical path will delay the reaching of the terminal event by at least the same amount.


PERT is often related with Critical Path Managment method https://en.wikipedia.org/wiki/Critical_path_method and has inspired different methods like the Theory of Contraints https://en.wikipedia.org/wiki/Theory_of_constraints

For an implementation example of PERT, see at: https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique


V-Model
=======


.. image:: images/VModel.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: V-Model
    

Ref : https://en.wikipedia.org/wiki/V-Model


The V-model is a graphical representation of a systems development lifecycle.

The following objectives are intended to be achieved by a project execution:
  - *Minimization of project risks*: The V-model improves project transparency and project control by specifying standardized approaches and describing the corresponding results and responsible roles. It permits an early recognition of planning deviations and risks and improves process management, thus reducing the project risk.
  
  - *Improvement and guarantee of quality*: As a standardized process model, the V-Model ensures that the results to be provided are complete and have the desired quality. Defined interim results can be checked at an early stage. Uniform product contents will improve readability, understandability and verifiability.
  
  - *Reduction of total cost over the entire project and system life cycle*: The effort for the development, production, operation and maintenance of a system can be calculated, estimated and controlled in a transparent manner by applying a standardized process model. The results obtained are uniform and easily retraced. This reduces the acquirer's dependency on the supplier and the effort for subsequent activities and projects.
  
  - *Improvement of communication between all stakeholders*: The standardized and uniform description of all relevant elements and terms is the basis for the mutual understanding between all stakeholders. Thus, the frictional loss between user, acquirer, supplier and developer is reduced.
    
The two streams
---------------

Specification stream:
  - User requirement specifications. Describes what the user need
  - Functional requirement specifications. Describes functionality identified to satisfy the user needs.
  - Design specifications. Describes how to make something to satisfy the functionalities.

Development:
  - configuration/customization
  - coding 
  
Testing stream:
  - Installation qualification. Can the product be installed/deployed without errors, so that the design is correct enough ?
  - Operational qualification. Does the product operate as expected, satisfying all functional requirements ?
  - Performance qualification. Is the product performant enough to satisfy the user needs ?

Looping back :
  - "Verification. The evaluation of whether or not a product, service, or system complies with a regulation, requirement, specification, or imposed condition. It is often an internal process. Contrast with validation." Ref PMBOK
  - "Validation. The assurance that a product, service, or system meets the needs of the customer and other identified stakeholders. It often involves acceptance and suitability with external customers. Contrast with verification." Ref PMBOK
  


Where to go from here ?
=======================

Around here:
 - :doc:`triangle`
 - :doc:`business`
 

Wikipedia:
 - https://en.wikipedia.org/wiki/Timeline_of_project_management
 - https://en.wikipedia.org/wiki/V-Model
 - https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique
 - https://en.wikipedia.org/wiki/Critical_path_method
 - https://en.wikipedia.org/wiki/Theory_of_constraints
 - https://fr.wikipedia.org/wiki/Project_Management_Body_of_Knowledge
 - https://en.wikipedia.org/wiki/Scientific_management
