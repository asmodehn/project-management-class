Business Considerations
=======================

Some Projects happen inside a company, that already has resources available to execute the project.

But more and more often, people want to address a problem and execute on a project in a more independently manner, with more **limited resources**.

In this case, one needs to consider all kinds of different aspects of a business in order to survive on uncertain markets among the competition.



Business Model Canvas
---------------------

Complete **Business plans can be very long and tedious** to produce. They might be useful for banks and investors, but more and more people find the little information they contain is lost in a lot of noise.

In an attempt to get a **clear picture of a business efficiently** and in a much shorter time, some people started developing alternatives. One of them is called Business Model Canvas, developed by Alexander Osterwalder.

+-------------+-------------+---------------+----------------+------------+
|Key Partners | Key         | Value         | Customer       | Customer   |
|             |             |               |                |            |
|             | Activities  | Propositions  | Relationships  | Segments   |
|             +-------------+               +----------------+            |
|             | Key         |               | Channels       |            |
|             |             |               |                |            |
|             | Resources   |               |                |            |
+-------------+-------------+-------+-------+----------------+------------+
| Cost Structure                    | Revenue Streams                     |
|                                   |                                     |
|                                   |                                     |
+-----------------------------------+-------------------------------------+

Key Partners:
 - Who are our Key Partners?
 - Who are our key suppliers?
 - Which Key Resources are we acquiring from our partners?
 - Which Key Activities do partners perform ?
 
Key Activities:
 - What Key activities do our Value Propositions require?
 - Our distribution Channels?
 - Customer Relationships?
 - Revenue streams?
 
Key Resources:
 - What Key Resources do our Value Propositions require?
 - Our Distribution Channels ? 
 - Customer Relationships?
 - Revenue Streams

Value Propositions:
 - What value do we deliver to the customer?
 - Which one of our customer's problems are we helping to solve ?
 - What bundles of products and services are we offering to each Customer Segment ?
 - Which customer needs are we satisfying?
 
Customer Relationships:
 - What type of relationship does each of our Customer Segments expect us to establish and maintain with them?
 - Which ones have we established?
 - How are they integrated with the rest of our business model?
 - How costly are they ?

Channels:
 - Through which Channels do our Customer Segments want to be reached?
 - How are we reaching them now?
 - How are our Channels integrated?
 - Which ones work best?
 - Which ones are most cost-efficient?
 - How are we integrating them with customer routines?

Customer Segments:
 - For whom are we creating value ?
 - Who are our most important customers?
 
Cost Structure:
 - What are the most important costs inherent in our business model
 - Which Key Resources are most expensive?
 - Which Key Activities are most expensive?

Revenue Streams:
 - For what value are our customers really willing to pay?
 - For what do they currently pay?
 - How are they currently paying?
 - How would they prefer to pay?
 - How much does each Revenue Stream contribute to overall revenues?

Before writing a full business plan, it is very enlightening to iterate quickly on many versions of business model canvas. It can help to quickly eliminate non-viable, or too risky business ideas before putting in the work for a full business plan.


Business Plan
-------------

A business plan is a formal **written document** containing business **goals**, the **methods** on how these goals can be attained, and the **time frame** within which these goals need to be achieved. It also describes the **nature of the business**, **background information** on the organization, the organization's **financial projections**, and the **strategies** it intends to implement to achieve the stated targets. In its entirety, this document **serves as a road map** that provides direction to the business.

The format of a business plan depends on its presentation context. It is common for businesses to have three or four formats for the same business plan:

- An elevator pitch. A teaser from 30 seconds to 60 seconds
- A pitch deck. Slideshow/ oral presentation to trigger interest and discussion
- A written presentation, nicely formatted plan targeted at external stakeholders
- An internal operational plan describing *all* planning details needed by management, but probably not interesting to external stakeholders. It can also be quite informal.

The content of a business plan depends on the industry, the market, and the whole context where the business will evolve.
It usually includes a lot of analysis of various factors ( market, competitors, business environment, etc.) and envision a plan for the various functions of the company (marketing, operations, finance, etc.)


Wardley Maps
------------

What is rarely explicited clearly in a business plan, and is of vital importance, is the understanding, by the project stakeholders, of what situation there find themselves, in their market, at this specific point in time.

There are analysis, but these are mostly written in natural language prose and lack formalism to convey unambiguously the complex understanding of the business situation. 

Wardley Maps are a useful formalism to represent it, becuase the meaningful information is not drowned in verbal noise.
They enable informed discussion between the different stakeholders because the understanding is visual and quick, so information is transferred efficiently between people.

These have been developped by Simon Wardley, and he has a book online where he details everything you need to know to start making your own maps.


Where to go from here ?
-----------------------

Around here:
 - :doc:`pppp`
 
Wikipedia:
 - https://en.wikipedia.org/wiki/Business_Model_Canvas
 - https://en.wikipedia.org/wiki/Business_plan

Internet:
 - https://medium.com/wardleymaps


