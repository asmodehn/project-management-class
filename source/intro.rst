************
Introduction
************

DISCLAIMER:
  The discussion here is written in order to trigger discussions between the students and the teacher(s). We aim to cite references for further details where possible. In the absence of reference, the discourse expressed here is solely the view of the author.  

Definition
----------

A project is something we decide to "execute" to achieve a *goal*.
It is something that is bound in time: it will probably *start*, and eventually (but definitively) *end*.

Project management is the practice of initiating, planning, executing, controlling, and closing the work of a team to achieve specific goals and meet specific success criteria at the specified time. 

Two questions must then be formulated: 
  - How will we recognize *when* it has been achieved ?
  - How will we *measure* what has been achieved ?

There is a tendency in the world to see every endeavour, every labor, as a project.
This comes from the industrial revolution and it has been quite successful so far.
But not everything is a project.

Examples:
  - "Cooking" is *not* a project.
  - "Cooking diner tomorrow" is a project.  A measurable goal (size of a meal), a probable start and an certain termination (after diner time tomorrow).
  - "Writing code" is not a project.
  - "Compute factorial for numbers between 1 to 9999" is a project. A measurable goal, a probable start and an eventual end (math tells us this ends).
  - "Provide a service that compute factorial" is not a project. Measurable goal, but no end in sight. How about maintenance to keep this service online ? How about factorial of very large numbers ?

If a tentative "project" does not clearly state a goal and an end, it usually means that the project is underspecified.
Or sometimes it is a not a project at all.
This is VERY common for software, a bit less for more mature industries.

In a similar, but more obvious way, a project with no resources (capital, time, people, etc.) is stillborn.


Art or Science ?
----------------

A comparison:
  - A Cook can make recipes he aready know, based on his experience, and the food can be pretty nice. But he will usually not come up with transformative recipes that change the "status quo" of what a meal is.
  - A Chef can come up with new recipe all the time, reinventing meals from first principles. Food might not always be to your liking, but he knows why it taste this way and how flavours combine to deliver the taste he wants.

There are few attempts to build a science corpus around project management (PMI, <n2?>), and often people try to market themselves as "chef". However this field is really new, and more alike social science, with a lot of relative and subjective arguments, than hard science (math, physics and biology somewhat) accepted by all and relatively easily verifiable everyday, to the point it has evolved practices about "how to do science" (hypothesis, verification, proof, even double-blind, etc.)

So experience is still the main skill required when managing a project, and usually undervalued. Certificates will not beat years in the field managing project constraints on a daily basis.
It ia also a domain that is very human centric, therefore subject to human culture, with all its biases and potentiel flaws...
In this way it is more alike sociology or psychology : A set of practices that we have found useful in previous occasions.
But it is not a set of absolute laws that we should apply blindly. 


Human Biases
------------

We need to be careful when discussing these concepts, because we are "living in them" and we are affected by them and by the decisions we make everyday, based on the stories we tell ourselves. Humans have many `Cognitive and Behavioral Biases <https://en.wikipedia.org/wiki/List_of_cognitive_biases>`_ that have been studied over the years. After learning more about them, we start to see these biases at play in our human organisations, and Project Management is no exception.


Where to go from here ?
-----------------------

Around the corner:
  - :doc:`history`
  - :doc:`pppp`
  - :doc:`biases`
  - :doc:`triangle`
  
Wikipedia:
  - https://en.wikipedia.org/wiki/Project_management
  - https://en.wikipedia.org/wiki/ISO_21500
  - https://en.wikipedia.org/wiki/Project_Management_Body_of_Knowledge
  - https://en.wikipedia.org/wiki/SWEBOK
  - https://opentextbc.ca/projectmanagement






