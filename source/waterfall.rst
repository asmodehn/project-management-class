Waterfall
=========

The "waterfall model" originated in manufacturing and construction industries, and then a whole group of people (project managers) with little knowledge of software, tried to apply the same methods in the growing software development industry, and attempted to make reality fit their mental model of what a project should be. At the time there weren't any widely recognised alternatives for knowledge-based work.

- https://en.wikipedia.org/wiki/Waterfall_model

"The first known presentation describing use of such phases in software engineering was held by Herbert D. Benington at the Symposium on Advanced Programming Methods for Digital Computers on 29 June 1956"

"The first formal description of the waterfall model is often cited as a 1970 article by Winston W. Royce, although Royce did not use the term waterfall in that article. Royce presented this model as an example of a flawed, non-working model"

Model
-----

In Royce's original waterfall model, the following phases are followed in order:

    - **System and software requirements**: captured in a product requirements document
    - **Analysis**: resulting in models, schema, and business rules
    - **Design**: resulting in the software architecture
    - **Coding**: the development, proving, and integration of software
    - **Testing**: the systematic discovery and debugging of defects
    - **Operations**: the installation, migration, support, and maintenance of complete systems

Thus the waterfall model maintains that one should move to a phase only when its preceding phase is reviewed and verified. 


Gantt Chart
-----------

.. image:: images/GanttChartAnatomy.png
    :width: 200px
    :align: center
    :height: 100px
    :alt: V-Model


Ref: https://en.wikipedia.org/wiki/Gantt_chart

A Gantt Chart provide a nice and intuitive view of a project estimated schedule.

One should be careful however, as :

 - It is not trivial to show more than a few dependencies, and therefore does not provide an accurate picture on complex projects.
 
 - Actual Progress can be very different that what the chart displays, for various reason. It is a simplistic model, and it is useful during planning, but it becomes very quickly outdated and useless for day-to-day management.
 

Where to go from here ?
-----------------------


Around the corner:
  - :doc:`classics`


Wikipedia:
 - https://en.wikipedia.org/wiki/Waterfall_model
 - https://en.wikipedia.org/wiki/All_models_are_wrong

