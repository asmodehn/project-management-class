Project Management Triangle
===========================


.. image:: images/ProjectTriangle.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: Project Management Triangle
    

A Project must find a balance between at minimum **three main constraints** (the PMBOK has recently extended these constraints) : Scope, Time and Cost

  - If you increase the Scope, Time and Cost will also have to increase.
  - If you decrease the Time, Scope will have to decrease, and Cost could potentially increase.
  - If you decrease the Cost, Scope will have to decrease, and Time could potentially decrease. 


These aspects are usually involved during negotiation between stakeholders (people who have invested resources or are directly interested in the outcome) and workers (people who are working on the completion of the project). Note that the two set of people are usually not totally disjoint...

To visualize from a high level, a tool such as a few GANTT Charts can be useful, but we should be careful to not forget important details in oversight.
This happen when people have little experience in Project Management, and can be costly for the organisation as a whole.
Indeed the **TopToBottom view** usually taken - cf. Waterfall - **is very different from the BottomUp practical approach**  - cf. Agile - and often hides critical details.

Here we discuss the classical three Project constraints in a bit more details. These are usually meaningful and reasonably well understood in both theoretical and practical approaches.


Scope
-----

A Project aims to **achieve a change in our direct environment**.
That change needs to be **clearly specified**, in attainable goal, otherwise it can end up being a never ending quest.

In general there is two angles to consider when specifying the project details:
 - **What we know needs to be done**: the work - the recipe
 - **How we will recognize it has been done**: the test - the concept

For example: A broadly accepted way to measure that scope for Software project are "Features", which are human understandable "things" the software can do, or the service can provide.

"Features" can be interdependent, so it is very important to have experience in how software features can be interdependent of each other.

Ideally the goal is to turn each "Feature" into a "mini-project", so that we can track its progress and verify its completion in the same way we do the whole project...

A few definitions:

  - **Tasks**: the different chunks of work that need to be completed. Varies broadly depending on the domain and kind of project. Care should also be taken about how to recompose these fragmented piece of work to achieve the overall project.
  
  - **Dependencies**: How different tasks are related together. It might make sense to complete T3 before being able to complete T2.
  
  - **Constraints**: Usually imposed from the start (often not explicitely enough), these can be functional - like "the final outcome should do this and NEVER do that" -, or technical - like " we want to use Technology T from our partner for political reason" -.


Time
----


A Project is "something" that has **a begin and an end**. In that delimited timeframe, a set of targets (Scope) must be reached, using some amount of resources (Cost)

If there is no clear end in time, it needs to have a very clear scope, which can be used to verify completion somehow.

Most IT projects get delayed because defining scope clearly is very difficult, and, if we want to do it correctly, requires mathematical tools and knowledge that are often very expensive.


  - **Effort**: the amount of work, or the number of work units used to complete an activity. Usually expressed in man.hours 
  
  - **Duration**: the entire time taken to complete an activity, based on the resources allocated to the project. Usually expressed in work hours/days/weeks, not including holidays.
  
  - **Time Elapsed**:  the time between designating a resource to a task and the completion of the task. Basically the passing of time as in "calendar days". Usually expressed with "usual calendar concepts" days/weeks.


Cost
----

When using the term "Cost" we mean the **resources that will be consumed** (as in "not available any longer") after their allocation to the project.
Note there are a lot of debates between people and cultures about what can be considered a cost (environment degradation, worker overtime, etc.). This will highly depend on how the company is managed. 

One has to be careful what is considered a cost, for various reasons:

- **Accountability** : Are the breakfast offered during meetings, a meaningful cost to consider ?
- **Sustainability** : Is the fact that doing something simple takes more and more time, a meaningful cost to consider ( aka technical debt ) ?  
- **Ethics** : Are the amount of people who quit or burnout a meaningful cost to consider ?
- etc.

Often, most of the costs are not clear at the beginning, but appear during the execution of a project.
**Identifying these and communicating** about it is key to avoid problems later on.


When thinking about resources, we should see three main aspects :

  - **Price**: how much it will cost us immediately, in currency (usually coming from company's assets). This will tell us if we are able to afford it, and if not can trigger us to action.
  
  - **Value Source**: how much we will benefit from it. What will we gain from accessing/owning it. It can be increase of productivity, investment that improves our financial stability, prestige that improves our potential atractivity for new hires, etc.
  
  - **Value Sink**: How much extra cost will be incured in the future ? It can be recurrent cost like a subscription, the cost of loss of productivity 



More?
-----

More **constraints will usually be identified while working on a project**.
Some will be occasional and with small inpacts, and can be dealt with when they occur.

Some will have greater impact or high reoccurence and will need the organisation to review and adapt its project management practices to the reality of the environment in which it finds itself.



Where to go from here ?
-----------------------

Around here:
  - :doc:`classics`
  - :doc:`waterfall`
  - :doc:`agile`
 
 
Wikipedia:
  - https://en.wikipedia.org/wiki/Project_management_triangle
  - https://en.wikipedia.org/wiki/Gantt_chart


