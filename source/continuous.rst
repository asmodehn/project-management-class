**********
Continuous
**********

Agile practices drive us to completely test everything and deliver as often as possible. When this reaches the point where literally **each version** is **tested and delivered** to the customer, this is called Continous Integration and Delivery (or CI/CD) 

Integration
===========

Continuous Integration requires automated testing, so that, as soon as a new version is detected (commited into your versioning system), then automatically tests are run, and the new feature is integrated into the whole, and some process verify that the whole is still working as it should.

Therefore it requires that your versioning system be related with machine able to build and test software (usually done on virtual machines or containers to make configuration and usage of these simple enough). It also enforces that everything required to build and test your software be available to the build machine (dependencies, tools, etc.).


Delivery
========

Continuous Delivery is the extra step, when each version is potentially sent to the client and put in production. The goal is to be very agile, and to get feedback as early as possible about potential problems in production systems, which often have different characteristics than your test machines.

However for this to be feasible and efficient, one need to think about the impact a potential error in the software can have, and for ways to limit it.

One strategy can be to quickly roll back to the previous version if a problem is detected. Another can be to slowly rollout the new version to new users, a few percentile at a time, to minimize hte number of customer impacted by the problem. 


Dependencies
============

To be able to achieve a continuous pipeline with maximum feedback it is necessary to rely on solid tools and practices, leveraging the various guarantees and capabilities of your versioning software.

Having a simple, obvious, and automated release system in place is also mandatory for this to not be too costly in engineering time.



Where to go from here ?
-----------------------


Around the corner:
  - :doc:`versioning`


Wikipedia:
 - https://en.wikipedia.org/wiki/Continuous_testing
 - https://en.wikipedia.org/wiki/Continuous_integration
 - https://en.wikipedia.org/wiki/Continuous_delivery

