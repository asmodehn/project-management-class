EPIC Fails
==========

“The fastest way to succeed is to double your failure rate.” - Thomas J. Watson Sr., Founder of IBM

While this quote is by no means dogma, nor a desirable way to obtain success, a plethora of companies have had to experience, and learn from, great failures. Finding and scrutinizing reasons for failure is a crucial part of the project management cycle.


Ford Edsel (1957)
-----------------

Ford did extensive market research before it released the Edsel, even doing studies to make sure the car had the right personality. But by the time all this research was done and the car was unveiled in 1957, Ford had missed its chance with its market, which had already moved on to buying compact cars, which didn’t include the Edsel.

“Even with the best planning and collaboration, things happen. Make sure your project schedule reflects the actual and current reality of the project.”

**Lesson**: Time To Market is key !


Apple Lisa (1983)
-----------------

Lisa, the first desktop with a mouse, cost $10,000 (almost $24,000 today) and had just 1 MB of RAM. Consumers weren’t as interested as Apple anticipated, and it was a case of over promising and under delivering, as the 1983 ads—featuring Kevin Costner—depicted the Lisa as much more than it really was.

**Lesson**: Communication and Transparency matter !


McDonald's Arch Deluxe Burger
-----------------------------

In 1996, McDonald’s put more than $150 million into advertising—more than it had ever spent on an ad campaign—for its new Arch Deluxe Burger, only to find out its customers weren’t interested in the more grown-up, sophisticated menu option.

**Lesson**: Stay close to your customer needs/wants -> tracking...


Betamax
-------

Start 1975 - End 2016 (!!!)

Betamax format was proprietary. VHS format is an open standard.
Eventually, VHS won.

https://en.wikipedia.org/wiki/VHS#Competition_with_Betamax

**Lesson**: Project management doesnt end when the product is delivered.


Ariane 5 1996
-------------

https://en.wikipedia.org/wiki/Ariane_5#Notable_launches

The software was originally written for the Ariane 4 where efficiency considerations led to four variables being protected with a handler while three others, including the horizontal bias variable, were left unprotected because it was thought that they were "physically limited or that there was a large margin of safety".

Probably human biases played a big role here...

**Cost**: 7 billion USD

**Lesson**: Plan carefully & Act responsibly -> clear & precise communication required !


Google Wave (2009)
------------------

Wave created a lot of hype and set high expectation during the demonstration at Google I/O in May 2009. It was marketed as a real time communication and collaboration platform that would be the “paradigm-shifting game-changer.”

However, in only a year, the project was discontinued due to the low user adoption.

One of the three pillars in project management determines the scope of the product, which deals with which features would meet its stakeholders’ requirements. The problem with Wave laid deeper than just the requirements: It didn’t have a clear target audience, whether businesses or consumers.

**Lesson**: Determine your target audience from the start to build solid user requirements.


IBM OS/360
----------

This was not per say a failure. However it was *very heavily delayed*.

**Lesson**: Adding manpower to a late software project makes it later.

https://en.wikipedia.org/wiki/History_of_IBM_mainframe_operating_systems#System/360_operating_systems

Group intercommunication formula = n(n-1)/2, with n the number of people involved. More channels means more chance to misunderstand something.

Ref : Mythical Man Month - Fred Brooks

https://en.wikipedia.org/wiki/Brooks%27s_law
https://en.wikipedia.org/wiki/Second-system_effect


BBC Digital Media Initiative (2008)
-----------------------------------

The DMI project was initiated by BBC in 2008 with an aim to modernize the company’s production and archiving methods by using connected digital production and media asset management systems. The project was abandoned in 2013 due to failures of governance and delayed delivery.

The business users decided not to stick to the agreed agile method which caused the CTO and his team to deliver big chunks of functionality instead of small, incremental ones. Due to the loose communication between the two teams, the business users were not satisfied.

In addition, the business users initially requested a feature that would allow them to produce a “rough cut” of video output. After it was developed, they changed their minds and decided to use an off-the-shelf product from Adobe. Even after the IT team integrated that specific Adobe product, the business users decided to use a different Adobe product altogether.

**Lesson**: Have a change control process in place to prevent scope creep


FBI’s Virtual Case File (2000 - 2005)
-------------------------------------

https://en.wikipedia.org/wiki/Virtual_Case_File

**Cost**: 104 million USD

**Lesson**: Delivering earlier means dropping features



The NHS’ Civilian IT Project
----------------------------

The project aimed to revolutionise the way technology is used in the health sector by paving the way for electronic records, digital scanning and integrated IT systems across hospitals and community care. It would have been the largest civilian computer system in the world.

Unrealistic expectations of both timelines and costs were not helped by inadequate preliminary research, failure to conduct progress reviews, and a clear lack of leadership. The project has been referred to as the ‘biggest IT failure ever seen’ and ‘a scandalous waste of taxpayers money’.

While the benefit of hindsight elucidates how the politically motivated nature of the top-down project was never going to suit the localized needs of the NHS divisions, it is yet to be seen whether the ambitious project will ever be re-attempted.

**Cost** : 10 billion GBP


Denver International Airport’s Automated Baggage System (1991)
--------------------------------------------------------------


In 1991, DIA attempted to remodel and fully automate the arduous, time-consuming luggage check-in and transfer system. The idea involved bar-coded tags being fixed to each piece of luggage that went through ‘Destination Coded Vehicles’.

When DIA contracted BAE Systems to develop the automated baggage handling system, they completely ignored BAE’s projected timelines, instead stubbornly sticking to their unrealistic 2-year schedule. The project was underscoped, and management took on unnecessary amounts of risk. Perhaps the most detrimental decision was to not include the airlines in the planning discussions. By omitting these key stakeholders, features catering to oversized luggage, sport/ski equipment racks, and separate maintenance tracks were not designed appropriately or at all.

Large portions of ‘completed’ works had to be redone, the airport opening was delayed by 16 months, and losses of approximately $2 billion were incurred. The entire project was scrapped in 2005.

**Cost**: 2 billion USD

**Lesson**: Customer need to be included in the discussions.


Where to go from here ?
-----------------------

Around the corner:
  - :doc:`history`
  - :doc:`pppp`
  - :doc:`biases`
  
Wikipedia:
 - https://en.wikipedia.org/wiki/List_of_failed_and_overbudget_custom_software_projects
 - https://en.wikipedia.org/wiki/History_of_IBM_mainframe_operating_systems#System/360_operating_systems
 - https://en.wikipedia.org/wiki/Brooks%27s_law
 - https://en.wikipedia.org/wiki/Second-system_effect
 - https://en.wikipedia.org/wiki/Virtual_Case_File

Internet:
 - https://web.archive.org/web/20140419020134/http://archive.wired.com/software/coolapps/news/2005/11/69355?currentPage=2

