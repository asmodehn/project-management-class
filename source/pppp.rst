
Plan - Process - People - Power
===============================

From a general, high-level perspective, what matter when embarking on a project are four key aspects:

  - Plan: The planning and forecasting activities.
  - Process: The overall approach to all activities and project governance.
  - People: Including dynamics of how they collaborate and communicate.
  - Power: Lines of authority, decision-makers, organograms, policies for implementation and the like.


Plan
----

This is not technical design. However it is often somewhat related.

We first need to assess our situation, and what we intend to do:
 - Where we are now, what is the market / society situation
 - What do we want to change
 - How do we plan to change it
 - How do we think the situation will evolve
 - etc.

Given the analysis we did, we come up with a **Plan**, to make expectations clear before we start:

 - What we will do, and how ?
 - Decompose the project into small chunks (work packages). 
 - What are these chunks ? how are they related ?
 - How do recomposing them afterwards is supposed to give us what we originally wanted ?
 - etc.

Process
-------

To eventually finish a large project, you ll need to follow one or more processes, that gives you some guarantee that you will finish.
To be able to continously run a **Process**, and verify its existence, you'll need to compose different smaller scale projects together.
In that case the higher level Process is called a **Program**.
All this terminology will depends of the industry, the environment and the culture of the people around.
Beware, in a language, words can change meaning very quickly.

The **Process** to execute to fulfill a project goal, is usually a sequence of actions to be executed over time.
Probably the most simple example is a cake recipe.
There are some guarantees in that process that it will lead us closer to our goal, and will eventually terminate.

More complex projects might require some intrically mixed **Processes** (sales, IT maintenance, etc.).
These are usually the inner structure of companies, and describe how they run.
Some companies are **Projects** (create, increase value, sell it !), other are **Processes** (old family company to generate livelyhood income, hopefully for ever)

This is indeed a bit like programming, but at a much larger scale, and with people involved.
We have mathematical theories to help us with machines, but people are complicated...

People
------

People are involved. People are not machines !
There are good sides and bad sides to both:

- availability
- efficiency
- improvability

But machine were made by people, so people perspective on things has been ingrained in them. We can see that in our tools.
Machines have been replacing people slowly, with some benefits, but some problems remain.

**Psychological biases** are probably the most important things to take in account when interacting with people, or with machines made by people.
Science and Humanities (History, Sociology, Psychology, etc.) can help us there, but it takes a long time to develop a science, and educate people about it.


Power
-----

Power has always played, and will keep playing, an important role.
The one who has power, can enforce his decisions, and hope to setup and monitor some processes, and maybe control some of the outcomes of a project.
Historically we had slaves that gave humanity the energy to build all our infrastructure, and the first machines.
Nowadays the machines are building themselves, using energy we harvest from the environment (Note: This is not an energy class).

People control Machines.  
Power control People.  

**=> Be aware of Power Structures** and their mechanics.
They will always have a part to play in a project.


