History
=======


Disclaimer:
This is a very incomplete and stereotypical discussion of history, with the purpose of illustrating a point.

For a much more accurate information about the Management of Projects throughout history, you can refer to `wikipedia <https://en.wikipedia.org/wiki/Project_management#History>`_ and there is the `History of Project Management <https://historyofprojectmanagement.com/>`_ book by Mark Kozak-Holland.


.. image:: images/Great-Pyramid-of-Egypt.jpg
    :width: 320px
    :align: center
    :height: 240px
    :alt: Great Pyramid of Egypt


Antiquity
---------

A Project in the old times, as seen through our modern era lense, was usually organised more or less in this way:

  - An **Experienced Craftman or Architect** acquired resources in some way,
  - He had a **measurable target**, set by whom the resources came from, investor or customer.
  - Often had **a date by which the outcome of the project was expected** (a tool produced before next harvest for example). If the end was not always explicit, it was obvious from the target measurement: when it is reached, the project ends. 
  
Depending on the constraints on the project, he could decide to gather a few peers and apprentices, to work more on a project and less on another, to borrow resources from someone else, to hire a subcontractor, etc.

In this stereotypical context there are a few things that are somewhat implicit, and that we often fail to see:

  - The Craftman/Architect has **a deep knowledge of the domain** of the project. He has **a clear idea of what he is promissing** the customer and what he can/cannot deliver.
  - The Craftman/Architect **has most of the skills needed** to accomplish it, and the others can be acquired or delegated quickly enough.
  - The Customer has somewhat **an understanding of what he is asking**, or at least how to measure that the goal has been achieved.


It was like that mostly throughout the middle ages as well, but this could be hindsight bias manifesting itself again...

Industry
--------

With the **industrial revolution** came the need to organize a large number of endaevours, by **large groups of people**, on more and more technical domain of an **exploding complexity**.

Henry Gantt and Henry Fayol attempted to describe how to structure a productive human organization via scientific principles.
There are more details in `wikipedia <https://en.wikipedia.org/wiki/Project_management#History>`_ about this.
Their practices were very influential, and the main tool that is still largely used and abused today is the GANTT chart.

In this setting there are still a few implicit things:

  - We have a **good common understanding of what we want** at the end : There is no question about how we will measure the success of a project.
    A bridge that collapse is not a bridge, a lamp that doesn't provide light is not a lamp, etc.
    Only for the first prototype of a very transformative new technology we might want to ask ourselves if the project succeeded. (Is an electrical light a better candle ? These are known as "Research Projects" )

  - Producing/Copying a good or product is the main activity. Mostly, **we already know what needs to be done**, and how it can be done when the project starts.
    The industrial endaevor usually requires much more resources than the resources that were used to come up with the new idea.


Modern Times
------------

"Software is eating the world" - Marc Andreessen

In this new setting the environment changed significantly:

  - There is **no longer common understanding** of what we want.
    The stakeholder or **the customer** may think they know what they want, but they **usually have no idea** of what they actually need.
    
  - We have some ways to make bring everyone on the same page about what is required and **how we will measure** it.
    But it **is far from obvious or simple**, and currently requires complex maths to express it (formal specifications).
    Our spoken language didn't have time to evolve for everyone to understand the technical or scientific concepts required, and usual "marketing practices" are not helping.

  - Producing the physical thing is no longer the part that take the most time, or even sometimes require the most investment.
    The **Research and Development work to do is potentially much costlier**, especially if we want some measure of quality.
    However investment is still directed to the last, most immediate, "customer facing" part of an industry.

For example the commonly spoken language definition of words like "Function", "Effect", "Process", "Channel" is not enough for software implementation.
Even highly educated engineers do not know the definition of these, and researchers are still discussing some of them.
It will take a few generations before enough of these concepts are distilled into our everyday language, like "website" that is now mostly understood by people with a bit of technical background.

Our direct environment, and how we should manage multiple-people endaevours is always changing, even more nowadays.


Where to go from here ?
-----------------------

Around the corner:
  - :doc:`pppp`
  - :doc:`biases`
  - :doc:`triangle`
  - :doc:`fails`
  
Wikipedia:
  - https://en.wikipedia.org/wiki/Project_management#History
  - https://en.wikipedia.org/wiki/Timeline_of_project_management
  - https://en.wikipedia.org/wiki/Project_Management_Body_of_Knowledge

The Internet:
  - https://www.standishgroup.com/sample_research_files/CHAOSReport2014.pdf
  - http://www.guerrillaprojectmanagement.com/the-chaos-report-myth-busters
  - https://a16z.com/2011/08/20/why-software-is-eating-the-world/


