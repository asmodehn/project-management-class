******************
Release Management
******************


Whether Development is done in a waterfall or in a more agile way, "something" needs to be delivered eventually. It can be a piece of software, a working service (ex: website deployed somewhere), a full scale information system (HW + SW), etc.

Since Project Management, and gathered expertise over the years, tells us to divide the work and deliver in increments as much as possible, Engineershad to come up wiht a way to "name" various increment. These are called "version".

Although the version number were meaningful when used between tech people and in small teams, they quickly became a "marketing message" to be twisted around to try to attract more customer, in effect removing any meaning behind the version itself (Think about the various Windows version names).

Eventually technical people still needed to refer to different versions in a stricter way, and came up with mainly two ways.


Semantic Versioning
-------------------

Semantic Versioning make sense in a very technical, rigourous context.

The version is a list of 3 numbers:
 - Major version
 - minor version
 - patch
 
usually displayed with dots in between : M.m.p such as 2.12.54

There exists a lot of variant that can depend on the project, and how it is structured internally. The point if that the semantics is useful for a user of this piece of software, as they represent degrees of compatibility.

 - A change of **major** versions will have **too many changes to anticipate consequences**, so one cannot be replaced by another. Potentially heavy changes in the user code base are required
 - A change of **minor** version is potentially disturbing, but the **main features remain untouched**, so replacing one with the other should not create too much trouble, in theory...
 - A change of **patch** version is **very minimal**, no problem has been anticipated if the user update that software.

So it is a good idea when integrating a piece of software into another, to check its version number and understand the semantics behind it, as well as the impact for maintenance.

As someone who releases software, it is also important to pay attention to the versioning scheme you choose to use, and how to best communicate to your users the changes between releases.

As an example, Python packages are versioned that way, and although there are some differences and debates (what means v1 compred to v0 ?), it ensure a certain consistency in the way package dependencies are managed in a project. 


Date Versioning
---------------

Date Versioning makes sense in a very non-technical, relaxed context.

Its main advantage is that it communicates clearly the date of the release, and in a setting where the one making a choice has no technical knowledge, it is a simpler "should I keep the old one, or try the new ?" choice.

Usually with date versioning release there are also rules in place for support, meaning that someone who wants to keep the old version knows how long this version is supported by the provider.

A good example for this is the versioning system of Ubuntu. They also add a simple "name" for reference in context where numbers can be problematic (systems configurations, people's memory, etc.).




Where to go from here ?
-----------------------


Around the corner:
  - :doc:`continuous`


Wikipedia:
 - https://en.wikipedia.org/wiki/Release_management
 - https://en.wikipedia.org/wiki/Software_versioning




