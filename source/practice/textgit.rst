***************************
Project Practice - Git Text
***************************

To pratice management of a knowledge based project, it is important to become familiar with the tools and techniques useful in that context.

Versioning is the most prevalent tool for this, and git is a very famous implementation of it.

To keep things simple we will focus only on:
 - text files
 - shell scripts 


Goal
----

Implement a hierarchy of files that stores the lyrics for a famous poem/song.

Note: a script should also be created to verify "as you go" if the goal is reached or not.

**Releases : One release per chapter should be made**
You can use the release feature from gitlab, or simply **tag a revision from the master branch** with the release name/number.


**Constraint : A commit should contain only one line**.
This will keep this exercise meaningful, and help us practice working with commits in a repository, rather than just editing a text file.

A template repo for the practice is available here:
 https://gitlab.mde.epf.fr/asmodehn/git_practice

We will use gitlab base feature for a simple organisation of this little project's work.


Organisation
------------

There are many ways to organise the team for reaching the goal.
The main two ways are:

 - **centralized**: everyone works on the same repo, different branch.
 - **distributed**: everyone works on different repos, forking each other.

It is useful to practice with the different workflows on a very simple project, before diving into a more complex setup.

In both centralized and distributed ways, you should use **feature branching** strategy.

Hint : make sure you always double check **where, on which repository** you make changes. The URL is the source of truth, not what the page looks like. Everything looks the same, but they are all a bit different.


Centralized
-----------

The team leader/project owner should **fork** the repository, and all members of the team will work on this fork.

**Create issues and milestones** to describe the steps agreed to reach the goal.

**Everyone needs to clone** the repository to their local machine.
It is advised that everyone creates a "personal" branch, with their own name to avoid problems with other people's work.
Commits and Pushes can then safely be made to that branch, or any sub-branch (feature branches!) on the origin remote/server

**Execute the work as planned**:
 - commiting one line at a time
 - creating Merge Requests and merging them when the result is good (dont forget to run the test script)
 - make a release for each verse completed
 
**Make a quick retrospective with your team** about what went well and what went badly. Discuss about how you would like to organize your team to collaborate for a project using git.
 
Distributed
-----------

**All members of the team should fork** the repository, everyone will be working on his own fork.

**Create issues and milestones** to describe the steps agreed to reach the goal. *Note:* Even if everyone has his own fork, a central place for this little project management needs to be agreed upon between the team members.

**Everyone needs to clone** their own repository to their local machine.
It is advised that everyone creates a "personal" branch, with their own name to avoid problems with other people's work.
Commits and Pushes can then safely be made to that branch, or any sub-branch (feature branches!) on the origin remote/server

**Execute the work as planned**:
 - commiting one line at a time
 - creating Merge Requests and merging them when the result is good. Note : *Don't forget to run the script provided for testing*
 - make a release for each verse completed, slowly progressing towards the full text.
 
**Make a quick retrospective with your team** about what went well and what went badly. Discuss about how you would like to organize your team to collaborate for a project using git.



Where to go from here ?
-----------------------

Around here:
 - :doc:`../agile`
 - :doc:`../configuration`
 

Wikipedia:
 - https://en.wikipedia.org/wiki/Version_control
 - https://en.wikipedia.org/wiki/Comparison_of_version-control_software
 - https://git.wiki.kernel.org/index.php/GitSvnComparison
 
The Internet:
 - https://git-scm.com/docs

