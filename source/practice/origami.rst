Project Practice - Origami
==========================

To practice project management, we should first choose a domain accessible to many people, easy to grasp yet hard to master.
It should also be doable multiple times, in the amount of time available, in order to gain experience from this practice.

A Facilitator will be in charge of setting up the environment, managing possible observers, and keep the **Project Team** and the **Customer** on track during the practice.

Domain
-------

Plane Origami.
Reference available: Book. 

Estimated time for one project completion: 30 minutes to 1 hour 

Roleplay
--------

We can setup a practice project play with 4 people:

**A Customer**:
 - "I want a plane in M minutes"
 - Has *five* needs that the product should address
 - Present his needs in *three* words
 - Decides on project success from his perspective based on these needs.
 - Has no knowledge about the domain (no access to the reference)
 - talks with the **Manager** <Waterfall>

And the **Project Team**:

**A Manager**:
 - "We will make a plane for you in E days"
 - reports to the customer (and potential observers) the current state of the project.
 - consumes resources (paper sheets) and provide it to the **Maker** and **Tester**
 - Has no knowledge about the domain (no access to the reference)
 - reports to the **Customer** <Waterfall>
 
**A Maker**:
 - *Actually makes* the plane
 - Has No resources
 - Has knowledge about the domain and access to the reference.
 - reports to the **Tester** <Waterfall>
 
**A Tester**:
 - *Verifies* the plane satisfy known customer needs
 - Has No resources
 - Has knowledge about the domain and access to the reference.
 - reports to the **Manager** <Waterfall>


Project Triangle
----------------

Scope:
  - The **Customer** has a list of needs to be met (decide on five needs before hand - be inventive !)
  - The **Customer** has a limited way to communicate ("three words" goal expressed to the **Manager**)
  - The **Manager** has to communicate/negotiate to grasp what the **Customer** actually wants/needs
  
  along with the *time* and *resource* constraints
  
Time:
  - The whole project happens in constrained time (environment)
  - The **Customer** can shorten this time as he pleases...
  - The **Customer** and **Manager** have to agree before hand on *when* they will meet to talk about the project, *how* this will be presented. Note: partial or complete "product" can be presented and evaluated by the customer.
  - The **Manager** can decide to use more time than estimated at first, but final score will be affected.
  
Resource:
  - The whole project happens with constrained resources (paper sheets available)
  - The **Customer** can arbitrarily reduce this resource as he pleases via one or more needs...
  - The **Customer** and **Manager** have to agree before hand on *what* will be required for the project.
  - The **Manager** can acquire more resources than estimated at first, but final score will be affected.


Waterfall
---------

Environment:
  - Setup Maximum Time
  - Setup Maximum Resources
  - **Customer** decide his *five* needs, secretely.
  - **Customer** publically announces what he needs in *three* words.
  
Project *starts*:
  - When Customer has expressed his needs

Needs: 
  - **Manager** meets with **Customer** to decide on project report frequency (every N minutes)
  - **Manager** estimates project scope, resource and time.
  - **Manager** meets with **Maker** and **Tester** to announce the project
  
Development:
  - **Maker** and **Tester** decide on *resources* and *time* required
  - **Manager** allocate *resources* and *time* slot
  - **Maker** starts producing something and iterates with the **Tester**.
  - In parallel, **Manager** must keep track of the project progression (based on *scope*, *resources* and *time*) 
  - In parallel, **Manager** can meet with **Customer** depending on their original agreement.

Validation:
  - Once **Tester** agree something is showable, announce it to the **Manager**.
  - Once the **Manager** agree something is showable, announce it to the **Customer**.
  - When presented with a product, **Customer** evaluates the result based on his *original needs*, via 1 to 5 stars.

Project *ends*:
  - Once the **Customer** is satisfied,OR
  - When the **Customer** gives up, OR
  - When Maximum Time has been reached, OR
  - When Maximum Resources has been consumed.

Note: Project can be paused if discussion is necessary. But it will impact final score.


Agile
-----

When everyone becomes more familiar with the "Project Practice", it is possible to modify some of the rules, provided that other parties agree to it. It must still try to get as close as possible to the real world, but allow various project management methods to be "tested" in this environment.

For example, to make a project more *agile*:
  - **Manager** can come up with a way to present the project status passively and have it continuously visible.
  - **Customer** can meet everyone else (but not access knowledge reference !)
  - Multiple partial products can be presented to the **Customer**
  - etc.


Observer Interactions
---------------------

It is the task of the Facilitator to manage observers.
It is useful to have observers watching while the team execute the practice, in a "Fishbowl" setup.

At the facilitator discretion, the project practice can be "paused", to allow discussion with one of the observers.



Project Evaluation
------------------

Customer:
  - How many of his five needs have been met ? [1..5]
  - How much of his work time has been used ? [1..5]

=> Marks apply to the whole Project team (**Manager**, **Maker** and **Tester**)


Customer + Project team mark:
  - How much of total available time has been used?  [1..5]
  - How much of time has been used compare to how much time was estimated to be needed ?  [1..5]
  - How much of total available resource has been used? [1..5]
  - How much of resources have been used compared to how much was estimated to be needed ? [1..5]
 


Where to go from here ?
-----------------------


Around the corner:
  - :doc:`../classics`


Wikipedia:
  - https://en.wikipedia.org/wiki/Fishbowl_%28conversation%29




