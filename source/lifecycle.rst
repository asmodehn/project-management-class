*****************************
Systems Development Lifecycle
*****************************


.. image:: images/SDLC.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: SDLC


In systems engineering, information systems and software engineering, the **systems development life cycle** (SDLC), also referred to as the application development life-cycle, is a process for planning, creating, testing, and deploying an information system. The systems development life cycle concept applies to a range of **hardware and software** configurations, as a system can be composed of hardware only, software only, or a combination of both. There are usually six stages in this cycle: requirement analysis, design, development and testing, implementation, documentation, and evaluation. 

In **project management** a project can be defined both with a project life cycle (PLC) and an SDLC, during which slightly different activities occur. According to Taylor (2004), **"the project life cycle encompasses all the activities of the project, while the systems development life cycle focuses on realizing the product requirements"**.

Phases
------

- **Preliminary analysis**: Begin with a preliminary analysis, propose alternative solutions, describe costs and benefits, and submit a preliminary plan with recommendations.

- **Systems analysis, requirements definition**: Define project goals into defined functions and operations of the intended application. This involves the process of gathering and interpreting facts, diagnosing problems, and recommending improvements to the system. Project goals will be further aided by analysis of end-user information needs and the removal of any inconsistencies and incompleteness in these requirements.

- **Systems design**: At this step, desired features and operations are described in detail, including screen layouts, business rules, process diagrams, pseudocode, and other documentation.

- **Development**: The real code is written here.

- **Integration and testing**: All the modules are brought together into a special testing environment, then checked for errors, bugs, and interoperability.

- **Acceptance, installation, deployment**: This is the final stage of initial development, where the software is put into production and runs actual business.

- **Maintenance**: During the maintenance stage of the SDLC, the system is assessed/evaluated to ensure it does not become obsolete. This is also where changes are made to initial software.

- **Evaluation**: Some companies do not view this as an official stage of the SDLC, while others consider it to be an extension of the maintenance stage, and may be referred to in some circles as post-implementation review. This is where the system that was developed, as well as the entire process, is evaluated. Some of the questions that need to be answered include if the newly implemented system meets the initial business requirements and objectives, if the system is reliable and fault-tolerant, and if it functions according to the approved functional requirements. In addition to evaluating the software that was released, it is important to assess the effectiveness of the development process. If there are any aspects of the entire process (or certain stages) that management is not satisfied with, this is the time to improve.

- **Disposal**: In this phase, plans are developed for discontinuing the use of system information, hardware, and software and making the transition to a new system. The purpose here is to properly move, archive, discard, or destroy information, hardware, and software that is being replaced, in a manner that prevents any possibility of unauthorized disclosure of sensitive data. The disposal activities ensure proper migration to a new system. Particular emphasis is given to proper preservation and archiving of data processed by the previous system. All of this should be done in accordance with the organization's security requirements.


Where to go from here ?
-----------------------

Around here:
 - :doc:`waterfall`
 - :doc:`agile`
 

Wikipedia:
 - https://en.wikipedia.org/wiki/Version_control
 - https://en.wikipedia.org/wiki/Comparison_of_version-control_software
 - https://en.wikipedia.org/wiki/Software_development_process
 - https://en.wikipedia.org/wiki/Systems_development_life_cycle
 
