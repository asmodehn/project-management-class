Advices
=======


Listening
---------

 - **Listen to the customer(s)**. And *understand* their problem before having any solution in mind. It will save you time down the road.
 - **Do not listen** to the customer when he/she talks about *solution*. Building it is *YOUR* job.

Competition
-----------

 - **Watch the competition**. There is information to see the customer problem from another angle.
 - **Stop worrying** about the competition. They're just people. Stop racing for the next new thing, maybe the point is somewhere else.
 - If competition makes more sense than what you re doing? Just take the ideas. Your job is to solve the customer problem, not to have a new idea.

Money
-----

 - **Get Paid**. Or you will not be able to work for long.
 - Pay attention to how the customer pays, and to what he/she pays for.
 - **Dont worry too much about getting paid**. Dont focus too much on avoiding risk, or you wont do anything new and great.
 - You need to "wow" the new customer, and keep building trust for existing customer.
 - You also need to appeal to customer emotional and social needs, not only functional.

Decisions
---------

 - **Speed up**. Make a decision, and Take action.
 - We focus on the cost to make the wrong decision or take the wrong action. But the cost of inaction, the cost of delay can be much greater.
 - **Say No**. Don't be afraid to say No, but say it for the right reasons. The job is to protect the customer.
 - Everytime you say yes to one thing, you say are saying no to **everything else**.
 
 
Tools
-----

 - **Do not focus on the tools**. They are just supposed to support human organisation around them, many successful team get by using just post-it notes on a big board.
 - **Focus on the concepts**. You can try various ways to organize your team, just with papers, postit and stickers on a visible location. If you think some of the paradigm embedded in the tools dont match the way the organisation functions, don't hesitate to change the tools. 
 
 
Vision
------

 - **Dont try to be a visionary**. Focus on solving problems for your customers, one at a time.
 - Be patient, clear Vision might come later.
 - **Be humble**, look at a situation through the eye of the customer.
 - Focus on the customer point of view, ask the difficult questions to your team.
 


Where to go from here ?
-----------------------


Around the corner:
  - :doc:`classics`


