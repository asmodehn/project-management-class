Cognitive Biases
================


.. image:: images/CognitiveBiasCodex.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: Cognitive Bias Codex


Humans have a lot of biases : Some way to see, reflect, react, think about things that might not make sense in the present, but have been "embedded" in us by our evolution and are sustained by our environment.
We cannot get rid of them, we should just be very self-aware, to not fall in their traps too often.
Having external feedback might be challenging, but it is helpful in forging an objective view of our behavior and actions.


Here we will discuss a few examples.


Decision making, belief, behavioural
------------------------------------

  - **Confirmation bias**: The tendency to search for, interpret, focus on and remember information in a way that confirms one's preconceptions.

  - **Hindsight bias**: Sometimes called the "I-knew-it-all-along" effect, the tendency to see past events as being predictable[58] at the time those events happened. 

  - **Anchor bias**: The tendency to rely too heavily, or "anchor", on one trait or piece of information when making decisions (usually the first piece of information acquired on that subject)

  - **Gambler's fallacy**: The tendency to think that future probabilities are altered by past events, when in reality they are unchanged. The fallacy arises from an erroneous conceptualization of the law of large numbers. For example, "I've flipped heads with this coin five times consecutively, so the chance of tails coming out on the sixth flip is much greater than heads."

  - **Optimism bias**: The tendency to be over-optimistic, underestimating greatly the probability of undesirable outcomes and overestimating favorable and pleasing outcomes

  - **Self-serving bias**: The tendency to claim more responsibility for successes than failures. It may also manifest itself as a tendency for people to evaluate ambiguous information in a way beneficial to their interests


Social biases
-------------

  - **Ingroup bias**: The tendency for people to give preferential treatment to others they perceive to be members of their own groups. 

  - **Actor-observer bias**: The tendency for explanations of other individuals' behaviors to overemphasize the influence of their personality and underemphasize the influence of their situation (see also Fundamental attribution error), and for explanations of one's own behaviors to do the opposite (that is, to overemphasize the influence of our situation and underemphasize the influence of our own personality). 

  - **Halo effect**: The tendency for a person's positive or negative traits to "spill over" from one personality area to another in others' perceptions of them

  - **False consensus effect**: The tendency for people to overestimate the degree to which others agree with them

  - **Group-serving bias**: The tendency for people of favoring members of their in-group over out-group members. This can be expressed in evaluation of others, in allocation of resources, and in many other ways.


How to mitigate these ?
-----------------------

These biases are very prevalent nowadays, and can be very damaging to a project progress. Maybe the main skill necessary to be a good Project Manager/Owner, is to be *very self-aware* and be a good judge of character.

There are many strategies to be able reduce the alea coming from these biases, some are just prejudice hiding under a "pseudo-scientific" layer, and other are real social/psychological science. Seeing the difference and educate people about it is where psychologist are useful.

The main one is the first step usually taken when attempting to do science:  monitoring different aspects of a project progress, including oneself, and attempt to see patterns and correlations. But be cautious : "When a measure becomes a target, it ceases to be a good measure" - *Goodhart's law*

  
Where to go from here ?
-----------------------

Around the corner:
  - :doc:`classics`
  - :doc:`software`


Wikipedia:
  - https://en.wikipedia.org/wiki/List_of_cognitive_biases
  - https://en.wikipedia.org/wiki/Goodhart's_law

