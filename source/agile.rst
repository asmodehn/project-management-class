Agile Methodologies
===================


Manifesto
---------

 - **Individuals and Interactions** over processes and tools
 - **Working Software** over comprehensive documentation
 - **Customer Collaboration** over contract negotiation
 - **Responding to Change** over following a plan 

However:
  “He who fails to plan is planning to fail.” — Winston Churchill


Scrum
-----

Scrum is an agile process framework **for managing complex knowledge work**, with an initial emphasis on software development, although it has been used in other fields and is slowly starting to be explored for other complex work, research and advanced technologies. It is designed for **teams of ten or fewer members**, who break their work into goals that can be completed within **timeboxed iterations**, called *sprints*, no longer than one month and most commonly two weeks, then track progress and re-plan in 15-minute time-boxed daily meetings, called daily scrums. 

Roles:
 - **Product Owner** : The product owner, representing the product's stakeholders and the **voice of the customer** (or may represent the desires of a committee), is responsible for delivering good business results. Hence, the product owner is accountable for the product backlog and for **maximizing the value** that the team delivers. The product owner defines the product in **customer-centric** terms (typically **user stories**), adds them to the Product Backlog, and prioritizes them based on importance and dependencies. **Communication is a core responsibility of the product owner**.
 
 - **Development Team**: The development team has from three to nine members who carry out all tasks required to build increments of valuable output every sprint. The team is **self-organizing**. While **no work should come to the team except through the product owner**, and the scrum master is expected to protect the team from too much distraction, the team should still be **encouraged to interact directly with customers** and/or stakeholders to gain maximum understanding and immediacy of feedback.
 
 - **Scrum Master**: Scrum is facilitated by a scrum master, who is **accountable for removing impediments** to the ability of the team to deliver the product goals and deliverables.[32] The scrum master is not a traditional team lead or project manager but acts as **a buffer between the team and any distracting influences**. The role has also been referred to as a team facilitator or **servant-leader** to reinforce these dual perspectives. 


Workflow:
 - **Sprint Planning**: At the beginning of a sprint, the scrum team holds a sprint planning event
 
 - **Daily scrum**: Each day during a sprint, the team holds a daily scrum (or stand-up) with specific guidelines
 
 - **Sprint Review and Retrospective**: At the sprint review, the team reviews the work that was completed and the planned work that was not completed. The team also presents the completed work to the stakeholders and decides what to work on next. At the sprint retrospective, the team reflects on the past sprint, then identifies and agrees on continuous process improvement actions
 
 - **Backlog Refinement** : Backlog refinement is the ongoing process of reviewing product backlog items and checking that they are appropriately prepared and ordered in a way that makes them clear and executable for teams once they enter sprints via the sprint planning activity.

Kanban
------

Kanban (Japanese 看板, signboard or billboard) is a **lean method to manage and improve work across human systems**. This approach aims to manage work by balancing demands with available capacity, and by improving the handling of system-level bottlenecks.

**Work items are visualized** to give participants a view of progress and process, from start to finish—usually via a Kanban board. Work is pulled as capacity permits, rather than work being pushed into the process when requested. 

As described in books on Kanban for software development, the **two primary practices** of Kanban are:

    - Visualize your work
    - Limit work in progress (WIP)

Four additional general practices of Kanban listed in Essential Kanban Condensed are:

    - Make policies explicit
    - Manage flow
    - Implement feedback loops
    - Improve collaboratively, evolve experimentally
    
In knowledge work and in software development, the aim is to provide a **visual process management system which aids decision-making** about what, when, and how much to produce. The underlying Kanban method originated in lean manufacturing, which was inspired by the Toyota Production System. Kanban is commonly used in software development in combination with other methods and frameworks such as Scrum.

**Lean software development** is a translation of lean manufacturing principles and practices to the software development domain.

Lean development can be summarized by seven principles, very close in concept to lean manufacturing principles:

 - Eliminate waste
 - Amplify learning
 - Decide as late as possible
 - Deliver as fast as possible
 - Empower the team
 - Build integrity in
 - Optimize the whole


eXtreme Programming
-------------------

Extreme programming (XP) is a software development methodology which is intended to **improve software quality and responsiveness to changing customer requirements**. As a type of agile software development, it advocates frequent "releases" in short development cycles, which is intended to improve productivity and introduce checkpoints at which new customer requirements can be adopted.

Other elements of extreme programming include:
 - programming in pairs or doing extensive code review
 - unit testing of all code
 - avoiding programming of features until they are actually needed
 - a flat management structure
 - code simplicity and clarity
 - expecting changes in the customer's requirements as time passes and the problem is better understood
 - frequent communication with the customer and among programmers.
 
The methodology takes its name from the idea that the beneficial elements of traditional software engineering practices are taken to "extreme" levels. As an example, code reviews are considered a beneficial practice; taken to the extreme, code can be reviewed continuously, i.e. the practice of pair programming.


Craftmanship Manifesto
----------------------

Software craftsmanship is an approach to software development that emphasizes the coding skills of the software developers. It is a response by software developers to the perceived ills of the mainstream software industry, including the prioritization of financial concerns over developer accountability. 

The Agile Manifesto, with its emphasis on "individuals and interactions over processes and tools" questioned some of these assumptions. The Software Craftsmanship Manifesto extends and challenges further the assumptions of the Agile Manifesto, drawing a metaphor between modern software development and the apprenticeship model of medieval Europe. 

    As aspiring Software Craftsmen we are raising the bar of professional software development by practicing it and helping others learn the craft. Through this work we have come to value:

 - Not only working software, but also **well-crafted software**
 - Not only responding to change,but also **steadily adding value**
 - Not only individuals and interactions, but also **a community of professionals**
 - Not only customer collaboration, but also **productive partnerships**

    That is, in pursuit of the items on the left we have found the items on the right to be indispensable.

            © 2009, the undersigned.

            This statement may be freely copied in any form, but only in its entirety through this notice




Where to go from here ?
-----------------------

Aroud the corner:
 - :doc:`classics`
 - :doc:`waterfall`

Wikipedia:
 - https://en.wikipedia.org/wiki/Agile_software_development
 - https://en.wikipedia.org/wiki/Software_craftsmanship
 - https://en.wikipedia.org/wiki/Lean_software_development
 - https://en.wikipedia.org/wiki/Scrum_(software_development)
 - https://en.wikipedia.org/wiki/Kanban_(development) 
 - https://en.wikipedia.org/wiki/Extreme_programming 


