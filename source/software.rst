****************
Software Project
****************


A lot of meaningful software are not projects, because there is usually no end in sight.
With more "obvious" fields, like building, tool making, etc. the measurement of the goal we want to achieve implies the end.
With software, measuring the goal is not straight forward, due to software complexity that no human alone can comprehend.

There are two possible solutions left:
  - **Let someone decide** if the target has been reached or not. Usually the one who brings resources feels entitled to have a say there, even if he is not the one most qualified or the one who will, in the end, benefit or suffer from the consequence.
  
  The team need to be careful here, because the subjectivity of the decision and a wrong perspective here can be very damaging to the project.
  
  - **Use even more complex tools** (Mathematical theories, Formal methods, Theorem proving, Testing) to objectively decide what must be reached and when it has been reached.
  
  These are still quite costly, and are usually justified only if the software is involved in a potentially life-threatening environment.
  And in any case, the stakeholders have to agree that the tools are relevant and appropriate to "delegate" decision power to them.


Project Management Triangle
---------------------------

In the specific case of Software:
  - **Scope**: Usually a set of Features to implement and Bugs to fix. It will quickly pay off to put in place, early at the beginning, some infrastructure in the code to establish common language and practices in the team about these changes. Different domain have different practices for this, so it will usually be on a case-by-case basis, but the goal is to establish early a "delivery pipeline" where actions required to deliver a new chunk of work ot the customers or shareholders are minimized. This will enable useful informed feedback from interested parties.
  
  - **Time**: Almost always underestimated. Because software is, by essence, a "research" profession (If you have done it before, just copy the code). Each new task is a learning process, and these are non-linear and non deterministic, therefore impossible to estimate precisely. So a potentially quite long time buffer should be negociated up front, and all the hard questions should be solved via research and prototyping before starting the project if possible.
  
  - **Cost**: Usually not that high to start (just get a computer, an internet connection, and you can start coding), but over the realization of the project, more and more unexpected needs will arise (user testing, documentation, art content, user experience consultant, security experts, etc.). Experience in the domain will be critical and budgeting these cost upfront would be essential to reach the project's goal.



Software Specification
----------------------

Given that deciding when a software project is "complete" seems to be a difficult problems, software engineers have, very early on, tried to write detailed specifications of a piece of software.

The PMI has, for any kind of project, a terminology for different level of granularity: Work Package / Activity / Task
There have been attempts to adopt something similar in the software industry : Functionality / Feature / Task

Specifications act as a kind of "contract" between software developer (provider) and the user (consumer) to determine what is required, and when it will be achieved. It is often related in some ways with the actual law-binding contract of a company, and therefore use *natural language*, with often mis-used "technical" words.

This has had very mitigated success so far, as everyone seems to agree on a project goal at the beginning, but as the project evolves, both party realize there were "details" missing. And it is very critical in software as:

- **software** technologies **evolves very quickly**
- **software is nothing but details** mixed up together. One detail missing and nothing works.
 
There is a spectrum of possibilities, and industries tend to collectively decide on which one is best for it. Two extremes could be:

- Formal specifications (Everything proven in math that it will work as intended) -> used for critical software. But it means **we know exactly** what we want and how to build it.

- Agile: The concept of integrating the customer directly within the software development team ( or vice versa ), and let time guide the software evolution. But it means you have a high risk to **not know exactly** what is the software, as it is now changing faster than you can understand it. Everyone needs to be more involved and proactive and this way of working is not always simple to interface with the traditional sequential way of working of many industries (documentation requirements, regulatory procedures, etc.)

Software development
--------------------

For fifty years or so, Software development used to be (in arguably the majority of cases) a lonely endaevor. The coder with his machine, unterstanding each other, was a couple difficult to separate.

However recently the community at large started to realize the importance of communication and **collaboration on software**. Trends are emerging : Pair Programming, Mob Programming are two examples.

The point is that if the code developed is understood by one person only, then it cannot evolve, cannot be changed, and therefore it is not sustainable.

We used to have documentation. But nobody reads manual for simple tools and manual for software tools are much bigger, and rarely up-to-date. So how can we expect a user to read and understand the manual ?

Documentation is still useful to keep track of the history of the development, to record why a choice was made instead of another one. Like a ship's captain log, the main purpose is to **educate other developers**. Should something unexpected happen (team gets fired, project gets cancelled, etc.), it can be used to recover knowledge and experience from the codebase.

Development then becomes an activity of shared learning between the developers. Learning about what needs to be done, what is possible with the current technology, what are the improvements needed, etc.

Also since multiple people will have different understanding of a domain, direct collaboration on writing the code is important, to confront each person's ideas with the capabilities of the system for which development is being done.

Quick iterations are keys, in order to maximise the use of the machine (the "source of truth") and minimise fruitless cognitive load on the people (attempting to simulate the computer in their brains).



Testing - Reviews - Continous Integration/Deployment
----------------------------------------------------

Many people have many different opinions on how to maximise the chance to successfully achieve a project, at least on the technical aspect.

However since the last decade or two, these have been more or less agreed upon by the broad community of software engineers:
 
 - **Testing** : Long time ago, computers used to be predictible, but this changed when we stopped writing C on PDP-11. Also the code that somebody else wrote (or even you 6 months ago) is not simple to understand and reason about. The main way to fix these issues is to **test, as much as possible**. Also it offers the beenefit to fix problems found during development phase, where it is much, much cheaper, than when the code is running in production.

 - **Code Review** : Simplicity is the best way to achieve a goal without unexpected issues. However when working on complex software in teams, time has shown that code tend to aggregate "accidental complexity", mostly because of lack of communication, or disagreement in the way developer's intention is expressed in the code. Code review is arguably the most agreed upon way **to increase understanding of the codebase** by all developers, and consequently to reduce accidental complexity.
 
 - **Continous Integration/Deployment**: Complexity is not only in code, it is also in the world around, and it is usually challenging to manage it. The way that many people agree is the most efficient is the Agile Mindset => Working software and Interactions (also with other systems!) are the core of the manifesto. This then lead directly to want to **integrate and deploy to the final environment (customer!) as often as possible**, and also obviously have a process in place to get feedback.

Note: Nowadays, more and more software developers are pushing towards Formal Methods as a way to do prototyping or verification of the properties of a piece of software. It is becoming more accessible and cheaper, and is already used in established industries where there is a high risk aversion (potential loss of life, etc.). It is however not enforced by law everywhere, especially in innovative products that *should* do it (pacemaker, etc.). 


Maintenance
-----------

As soon as a software is successful, it needs to be **maintained** by a group of people **to remain useful** over time.
It also often need to **renew and reinvent itself** to keep people interested in it enough to actively want to use it.

This is also a problem in recent, more technologically advanced, industry, and it has so far largely been solved by constantly producing&consuming, inflating the economy as an intended side-effect.

Do you buy a new car often ?
You probably dont need to know how it works:
- let the specialist handle it 
- let's get rid of it and the company will sell you a new one

More "traditional industry" (bridges, road infrastructure, etc.) has been maintained over a long period of time, because its maintenance cost has been mutualised and spread over a large group of people (usually via taxes).

Software industry on the other hand currently has an important problem of understanding how to become viable in the long term (more than one generation of engineers).

We will not dwell much longer on maintenance here, but we need to remember that a **Project** is just the tip of the iceberg, currently visible because we are focusing on it in the present. But it depends on a large, unseen, amount of work that was previously done, and that still need to be maintained to preserve it over time.

Contrary to a **Project**, maintenance is a potentially never ending process, and has the not so simple goal of not letting things become worse. However this is unconsciously taken for granted by people **Hindsight Bias**, but it still has a cost that needs to be met.

Recent Management practices tend to forget the necessary maintenance, to focus on "Innovation", but one cannot exist without the other.


Where to go from here ?
-----------------------

Around here:
 - :doc:`versioning`
 - :doc:`waterfall`
 

Wikipedia:
 - https://en.wikipedia.org/wiki/Software_development_process
 - http://www.melconway.com/Home/Conways_Law.html
 - https://en.wikipedia.org/wiki/Bus_factor
 

