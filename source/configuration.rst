************************
Configuration Management
************************

Despite the name, this is not only about the "configuration" of a piece of software, but about **tracking changes** (source code, dependencies, deployment settings, etc.) in a software project.

This nowadays is made by what is called "branching". It means splitting the flow of successive changes (revisions) in two distincts "paths". There are mainly two way to use this capability.


Release branching
=================

Release branching refer to the practice of "branching for a release". Meaning everytime that one plans to make a release, features are "locked" (we do not develop them any more), and we focus on identifying and fixing issues.

To do that efficiently a branch is created where we track only fixes, and any new feature or modification of something already working is effectively forbidden.

To get the fixes back into the "main line of developement", it is possible to take the commits in the release branch, and duplicate them in the main line development branch.


Feature branching
=================

Feature branching refers to the practice of "branching for a feature", which is somewhat complementary to release branching.

The practice here consists in  creating a new branch for every new feature, effectively keeping the main development line free of change, until the feature is "ready for merge". The changes for the features are then reintegrated with the main line of development and the branch is eventually deleted.

It is important to note that in this situation, the entire list of changes from the feature is completely "merged" into development


Combining both
==============

Some practices combine both of these approaches, like *git-flow* for example.

Feature branching happens freely during development. Then at some point, when it becomes suitable to make a release, a merge of the development branch is made into a "master" branch, signalling that this version is ready for release.

Upon release, the exact revision is "tagged" for later retrieval, and any "hotfixes", ie late bug fixes are commited into a newly created release branch.

Note : Having both a master branch for release and a development branch for feature integration ensures that one flow doesn't interrupt the other, and users can chose if they want the "latest and potentially unstable version from development" or the "safe and well tested version, even if a bit old".


Where to go from here ?
-----------------------


Around the corner:
  - :doc:`versioning`


Wikipedia:
 - https://en.wikipedia.org/wiki/Software_configuration_management
 - https://en.wikipedia.org/wiki/Branching_(version_control)
 
The Internet:
 - https://danielkummer.github.io/git-flow-cheatsheet/


