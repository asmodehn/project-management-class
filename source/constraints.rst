Theory of Constraints
=====================

The theory of constraints (TOC) is a management paradigm that views any manageable system as being **limited** in achieving more of its goals **by a** very small **number of constraints**. There is always at least one constraint, and TOC uses a focusing process to **identify the constraint** and **restructure** the rest of the organization **around it**.

TOC adopts the common idiom **"a chain is no stronger than its weakest link"**. This means that processes, organizations, etc., are vulnerable because the weakest person or part can always damage or break them or at least adversely affect the outcome. 

The management philosophy was introduced by Eliyahu M. Goldratt in his 1984 book titled The Goal, that is geared to help organizations continually achieve their goals. Goldratt adapted the concept to project management with his book Critical Chain, published in 1997. 

More recently "The bottleneck rules" by Clark Ching is a concise book distilling the key ideas of Theory of Constraints.

Key assumption
--------------

The underlying premise of the theory of constraints is that **organizations can be measured and controlled by** variations on three measures:

- **Inventory**: all the money that the system has invested in purchasing things which it intends to sell. 
- **Operational Expense**: all the money the system spends in order to turn inventory into throughput.
- **Throughput**: the rate at which the system generates money through sales.

Note the only units we are dealing with here are:

- money
- time 

This simplifies the view of a problem, and allow one to focus on identifying constraints that are currently in place. 

The five focusing steps
-----------------------

Theory of constraints is based on the premise that the rate of goal achievement by a goal-oriented system (i.e., the system's throughput) is limited by **at least one constraint**. Otherwise it would be infinite !

Therefore, only by increasing flow through the constraint can overall throughput be increased.

Assuming the goal of a system has been articulated and its measurements defined, the steps are:
 - **Identify** the system's constraint(s).
 - **Decide how to exploit** the system's constraint(s).
 - **Subordinate everything else** to the above decision(s).
 - **Alleviate** the system's constraint(s).
 - Warning! If in the previous steps a constraint has been broken, **loop back to Identify** step, **but do not allow inertia to become a constraint** => monitoring/observability.

There can be multiple constraint, but by focusing, we attempt at minimizing the number of constraints considered in one pass, in order to make it tractable.

The goal of a commercial organization is: "Make more money now and in the future", and its measurements are given by **throughput accounting**.

The five focusing steps aim to ensure ongoing improvement efforts are centered on the organization's constraint(s). In the TOC literature, this is referred to as the process of ongoing improvement (POOGI).


Constraints
-----------

A constraint is anything that prevents the system from achieving its goal. There are many ways that constraints can show up, but a core principle within TOC is that there are not tens or hundreds of constraints. There is at least one, but at most only a few in any given system. Constraints can be internal or external to the system.

Types of (internal) constraints:
 - **Equipment**: The way equipment is currently used limits the ability of the system to produce more salable goods/services.
 - **People**: Lack of skilled people limits the system. Mental models held by people can cause behaviour that becomes a constraint.
 - **Policy**: A written or unwritten policy prevents the system from making more.
 - etc.

In TOC, the constraint is used as a focusing mechanism for management of the system. The constraint is the limiting factor that is preventing the organization from getting more throughput (typically, revenue through sales) even when nothing goes wrong. 

**If a constraint's throughput capacity is elevated** to the point where it is **no longer the system's limiting factor**, this is said to **"break" the constraint**. The limiting factor is now some other part of the system, or may be external to the system (an external constraint).


Buffers
-------

Buffers are used throughout the theory of constraints. They often result as part of the exploit and subordinate steps of the five focusing steps.

Buffers are placed **before the governing constraint**, thus ensuring that **the constraint is never starved**. Buffers are also placed **behind the constraint** to **prevent downstream failure from blocking the constraint's output**. Buffers used in this way protect the constraint from variations in the rest of the system and should allow for normal variation of processing time and the occasional upset (Murphy) before and behind the constraint. 


System observability
--------------------

Overall the Theory of Constraints is a **set of tools and principles to help people manage humans organisation** using knowledge from system dynamics.

**System Dynamics** is a way to model a system with potentially very complex graphs, made up from **stocks**, **flows** and **feedback loops**, which is an intuitive way to model and visualize the evolution of a system.

Traditionnally for ToC, these systems have been factories, companies, and therefore most of its examples are related to money and physical production throughput. But its principles are far reaching, and could potentially be used with different goal or metrics, and be useful in other domains. 

It is striking how **PERT can be seen as over-simplified** and high-level, long time scale, **system dynamics**, where stock are binary and feedback loops are missing.

There is also a **strong similarity with the practices emerging from Software Engineering**, with buffers being automated tests, there to exercise the constraint, ie. the code in development, to maximize the throughput, ie the usefulness (for the end user) of the code.

There are similarities in all these models for a very good reason, that our brains, at the most basic level, have very similar structure, shaped by evolution, tend to interpret reality in the same way, and look for similar patterns everywhere. These aspects are currently being formulated in math research, and have already brought interesting results. If interested, you can dive into Applied Category Theory.


Where to go from here ?
-----------------------

Around the corner:
  - :doc:`software`
  - :doc:`biases`
  
Wikipedia:
 - https://en.wikipedia.org/wiki/Theory_of_constraints
 - https://en.wikipedia.org/wiki/The_Goal_%28novel%29
 - https://en.wikipedia.org/wiki/Bottleneck_(production)
 - https://en.wikipedia.org/wiki/Throughput_accounting
 - https://en.wikipedia.org/wiki/Continual_improvement_process
 - https://en.wikipedia.org/wiki/System_dynamics
 - https://en.wikipedia.org/wiki/Applied_category_theory



