***************
Version Control
***************

Software development is "knowledge work" and is usually a very complex endaevour. In this context, while developing software it is crucial that people can keep track of changes and when they happened.
It is also important that we can simply "go back in time" to be able to recover functionality that we may loose during development.
 
For these reason, very early software engineers developed version control systems, and they now start to be used in other fields. As soon as someone needs to use a computer for his work, it is a very powerful and convenient way to **never loose anything**. It minimizes risk.

Nowadays they have become prevalent everywhere tech is developed, and many software project management techniques implicitely rely on the guarantees it provides.

History
-------

There were, and still are, many different ways in which you can store versions of your code, usually just a set of text files.
The most famous free/open versioning systems nowadays are probably:

- 1986: CVS: Concurrent Versioning System
- 2000: Subversion
- 2005: Git | Mercurial


Subversion (SVN)
----------------

.. image:: images/Svntree.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: Svn Tree

Client-Server Model:
 In a client–server model, users access a master repository via a client; typically, their local machines hold only a working copy of a project tree. Changes in one working copy must be committed to the master repository before they are propagated to other users.
 
Concurrency addressed by Merge or Lock:
 In a lock model, changes are disallowed until the user requests and receives an exclusive lock on the file from the master repository. In a merge model, users may freely edit files, but are informed of possible conflicts upon checking their changes into the repository, whereupon the version control system may merge changes on both sides, or let the user decide when conflicts arise.


 - **Commits as true atomic operations** (interrupted commit operations in CVS would cause repository inconsistency or corruption).
 - Renamed/copied/moved/removed **files retain full revision history**.
 - **Versioning of symbolic links**.
 - Native support for binary files, with space-efficient **binary-diff storage**.
 - Apache HTTP Server as network server, WebDAV/Delta-V for protocol. There is also an independent server process called svnserve that uses a custom protocol over TCP/IP.
 - **Branching is** a cheap operation, **independent of file size** (though Subversion itself does not distinguish between a branch and a directory)
 - **Natively client–server**, layered library design.
 - Client/server protocol sends diffs in both directions.
 - **File locking for unmergeable files** ("reserved checkouts").
 - **Path-based authorization**.
 - **Full MIME support** – users can view or change the MIME type of each file, with the software knowing which MIME types can have their differences from previous versions shown.
 - **Merge tracking** – Merges between branches will be tracked, this allows automatic merging between branches without telling Subversion what does and does not need to be merged.


Git
---

.. image:: images/Git_operations.png
    :width: 320px
    :align: center
    :height: 240px
    :alt: Git Operations


Distributed Model:
 In a distributed model, repositories act as peers, and users typically have a local repository with version history available, in addition to their working copies.
 
Concurrency addressed by Merge only:
 In a lock model, changes are disallowed until the user requests and receives an exclusive lock on the file from the master repository. In a merge model, users may freely edit files, but are informed of possible conflicts upon checking their changes into the repository, whereupon the version control system may merge changes on both sides, or let the user decide when conflicts arise.

- **Strong support for non-linear development**: Git supports rapid branching and merging, and includes specific tools for visualizing and navigating a non-linear development history. In Git, branches are only references to a commit. With its parental commits, the full branch structure can be constructed.

- **Distributed development**: Like Mercurial and others, Git gives each developer a **local copy of the full development history**, and changes are copied from one such repository to another.

- **Compatibility with existent systems and protocols**: Repositories can be published via Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP), or a Git protocol over either a plain socket, or Secure Shell (ssh). Subversion repositories can be used directly with git-svn.
    
- **Efficient handling of large projects**: Fetching version history from a locally stored repository can be one hundred times faster than fetching it from the remote server.

- **Cryptographic authentication of history**: The Git history is stored in such a way that the ID of a particular version (a commit in Git terms) depends upon the complete development history leading up to that commit. Once it is published, it is not possible to change the old versions without it being noticed. (Mercurial also has this property.)

- **Toolkit-based design**: Git was designed as a set of programs written in C and several shell scripts that provide wrappers around those programs, and it is easy to chain the components together.

- **Pluggable merge strategies**: As part of its toolkit design, Git has a well-defined model of an incomplete merge, and it has multiple algorithms for completing it, culminating in telling the user that it is unable to complete the merge automatically and that manual editing is needed.

- **File Hierarchy Snapshots**: The earliest systems for tracking versions of source code worked on individual files and emphasized the space savings to be gained from delta encoding the (mostly similar) versions. Later revision-control systems maintained this notion of a file having an identity across multiple revisions of a project. However, Git does not explicitly record file revision relationships at any level below the source-code tree.

These implicit revision relationships have some significant consequences:

 - It is slightly more costly to examine the change history of one file than the whole project.
 - Renames are handled implicitly rather than explicitly. Git detects renames while browsing the history of snapshots rather than recording it when making the snapshot. However, it does require more CPU-intensive work every time the history is reviewed, and several options to adjust the heuristics are available. This mechanism does not always work; sometimes a file that is renamed with changes in the same commit is read as a deletion of the old file and the creation of a new file. Developers can work around this limitation by committing the rename and the changes separately.



Where to go from here ?
-----------------------

Around here:
 - :doc:`lifecycle`
 - :doc:`waterfall`
 

Wikipedia:
 - https://en.wikipedia.org/wiki/Version_control
 - https://en.wikipedia.org/wiki/Comparison_of_version-control_software
 - https://git.wiki.kernel.org/index.php/GitSvnComparison
 
