#########################
Project Management Basics
#########################

In here, we will:
 - get a quick overview of what a *Project* is and how it can be *managed*
 - go through the specificities of knowledge-based projects and especially *software*
 - *practice* project management via role-play on *tiny* *hands-on* projects 

.. toctree::
    :maxdepth: 2
    :numbered:
    :titlesonly:
    
    intro.rst
    history.rst
    fails.rst
    triangle.rst
    classics.rst
    business.rst
    pppp.rst
    biases.rst
    constraints.rst
    
    practice/origami.rst
    
    lifecycle.rst
    software.rst
    waterfall.rst
    agile.rst
    release.rst
    continuous.rst
    configuration.rst
    versioning.rst
    
    practice/textgit.rst
    
    
    advices.rst
    

Quick Search
============

* :ref:`search`

